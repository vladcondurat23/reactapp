import styled from 'styled-components';

export const IFContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 2px;
`;

export const IFTextWrapper = styled.div`
  font-size: 12px;
  color: white;
  padding-left: 2px;
`;

export const IFField = styled.input`
  width: 100%;
  height: 40px;
  border-radius: 8px;
  color: white;
  border: none;
  padding: 0 8px 0 8px;
  outline: none;
  background: rgba(255, 255, 255, 0.05);
`;
