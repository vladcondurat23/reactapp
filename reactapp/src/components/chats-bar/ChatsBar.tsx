import ChatCell from '../chat-cell';
import SearchBar from '../search-bar';
import { CBContainer, CBCellWrapper } from './styles';

const ChatsBar = () => (
  <CBContainer>
    <SearchBar />
    <CBCellWrapper>
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
      <ChatCell />
    </CBCellWrapper>
  </CBContainer>
);

export default ChatsBar;
