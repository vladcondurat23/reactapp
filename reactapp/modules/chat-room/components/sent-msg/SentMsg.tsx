import { SMContainer, SMMsgTime, SMTextContainer, SMWrapper } from './styles';

const SentMsg = () => (
  <SMWrapper>
    <SMContainer>
      <SMTextContainer>
        ce faci faci faci faci faci faci faci faci faci faci faci v v vfaci faci faci faci faci faci faci faci faci faci faci faci faci faci faci faci faci faci faci faci faci faci
        faci faci faci faci faci faci faci faci faci faci faci?
      </SMTextContainer>
      <SMMsgTime>10:00 PM</SMMsgTime>
    </SMContainer>
  </SMWrapper>
);

export default SentMsg;
