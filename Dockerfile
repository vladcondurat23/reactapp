FROM node:21 AS builder

WORKDIR /app

COPY ./reactapp/package.json ./reactapp/package-lock.json ./

RUN npm install

COPY ./reactapp ./

CMD ["npm", "run", "dev"]
